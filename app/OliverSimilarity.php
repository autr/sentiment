<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OliverSimilarity extends Model {
    protected $table = 'oliver_similarity';
    public $timestamps = false;

    protected $fillable = ['post_id', 'similar_id', 'similarity'];

    public function OriginalPost() {
        return $this->belongsTo('App\Sentiment', 'post_id');
    }

    public function SimilarPost() {
        return $this->belongsTo('App\Sentiment', 'similar_id');
    }
}
