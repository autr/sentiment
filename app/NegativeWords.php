<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NegativeWords extends Model
{
    protected $table = 'negative_words';
    protected $primaryKey = 'word';
    public $incrementing = false;
    public $timestamps = false;
}
