<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PositiveWords extends Model
{
    protected $table = 'positive_words';
    protected $primaryKey = 'word';
    public $incrementing = false;
    public $timestamps = false;
}
