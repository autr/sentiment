<?php

namespace App\Http\Controllers;

use App\Sentiment;
use Illuminate\Http\Request;

class StatisticsController extends Controller {
    public function index(int $type) {
        $types = array(
            0  => 'Showerthoughts 2013'
        );

        $sentimentTypes = array('simple', 'afinn', 'datumbox', 'twitter');

        $sentimentObj = new Sentiment();

        $sentimentStats = [];
        $sentimentSimilarity = [];
        foreach ($sentimentTypes as $sentimentType) {
            $sentimentStats[$sentimentType] = $sentimentObj->GetSentimentStats($type, $sentimentType);
            foreach ($sentimentTypes as $sentimentType2) {
                $sentimentSimilarity['overall'][$sentimentType][$sentimentType2] =
                    $sentimentObj->GetSentimentSimilarity($type, $sentimentType, $sentimentType2);

                $sentimentSimilarity['positive'][$sentimentType][$sentimentType2] =
                    $sentimentObj->GetSentimentSimilarity($type, $sentimentType, $sentimentType2, 1);

                $sentimentSimilarity['negative'][$sentimentType][$sentimentType2] =
                    $sentimentObj->GetSentimentSimilarity($type, $sentimentType, $sentimentType2, -1);

                $sentimentSimilarity['neutral'][$sentimentType][$sentimentType2] =
                    $sentimentObj->GetSentimentSimilarity($type, $sentimentType, $sentimentType2, 0);
            }
        }

        $topicStats = $sentimentObj->GetTopicStatistics($type);
        $languageStats = $sentimentObj->GetLanguageStatistics($type);
        $textStats = $sentimentObj->GetTextTypeStats($type);

        return view('statistics', [
            'type' => $type,
            'typeName' => $types[$type],
            'sentimentStats' => $sentimentStats,
            'sentimentSimilarity' => $sentimentSimilarity,
            'topicStats' => $topicStats,
            'languageStats' => $languageStats,
            'textStats' => $textStats
        ]);
    }
}
