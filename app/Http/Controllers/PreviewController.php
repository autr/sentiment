<?php

namespace App\Http\Controllers;

use App\Classes\DatumboxAPI;
use App\Classes\DictionarySentiment;
use App\OliverSimilarity;
use App\Sentiment;

class PreviewController extends Controller {
    private $apiKey;

    public function __construct() {
        $this->apiKey = env('API_KEY');
    }

    public function index(int $type) {
        $types = array(
            0  => 'Showerthoughts 2013'
        );
        $records = Sentiment::where('type', $type)->get(['id', 'title', 'topic', 'score']);

        return view('preview', [
            'type' => $type,
            'typeName' => $types[$type],
            'records' => $records
        ]);
    }

    public function detail(int $id) {
        $sentimentDictionary = array([
           'positive' => 1,
           'neutral' => 0,
           'negative' => -1
        ]);

        $datumBoxApi = new DatumboxAPI($this->apiKey);
        $dictionarySentiment = new DictionarySentiment();

        $error = false;

        $record = Sentiment::find($id);

        // Simple sentiment
        if (is_null($record->simpleSentiment)) {
            $simpleSentiment = $datumBoxApi->TopicClassification($record->title);
            
            $record->update(['simpleSentiment' => $simpleSentiment]);
        }

        // Afinn sentiment
        if (is_null($record->topic)) {
            $afinnSentiment = $dictionarySentiment->CountAfinnSentiment($record->title);
            $afinnSentimentSimplified = $dictionarySentiment->GetSimplifiedAfinnSentiment($afinnSentiment);

            $record->update(['afinnSentiment' => $afinnSentiment, 'afinnSentimentSimplified' => $afinnSentimentSimplified]);
        }

        // Topic classification
        if (is_null($record->topic)) {
            $topic = $datumBoxApi->TopicClassification($record->title);
            if ($topic) {
                $record->update(['topic' => $topic]);
            } else {
                $error = true;
            }
        }

        // Sentiment analysis
        if (is_null($record->datumboxSentiment)) {
            $datumboxSentiment = $datumBoxApi->SentimentAnalysis($record->title);
            if ($datumboxSentiment) {
                $record->update(['datumboxSentiment' => $sentimentDictionary[$datumboxSentiment]]);
            } else {
                $error = true;
            }
        }

        // Twitter sentiment analysis
        if (is_null($record->twitterSentiment)) {
            $twitterSentiment = $datumBoxApi->TwitterSentimentAnalysis($record->title);
            if ($twitterSentiment) {
                $record->update(['twitterSentiment' => $sentimentDictionary[$twitterSentiment]]);
            } else {
                $error = true;
            }
        }

        // Subjectivity analysis
        if (is_null($record->subjectivity)) {
            $subjectivity = $datumBoxApi->SubjectivityAnalysis($record->title);
            if ($subjectivity) {
                $record->update(['subjectivity' => $subjectivity]);
            } else {
                $error = true;
            }
        }

        // Spam detection
        if (is_null($record->spam)) {
            $spam = $datumBoxApi->SpamDetection($record->title);
            if ($spam) {
                $record->update(['spam' => $spam]);
            } else {
                $error = true;
            }
        }

        // Adult content detection
        if (is_null($record->adult)) {
            $adult = $datumBoxApi->AdultContentDetection($record->title);
            if ($adult) {
                $record->update(['adult' => $adult]);
            } else {
                $error = true;
            }
        }

        // Readability assessment - DOESN'T WORK
//        if (is_null($record->readability)) {
//            $readability = $datumBoxApi->ReadabilityAssessment($record->title);
//            $errorMessage = $readability;
//            if ($readability) {
//                $record->update(['readability' => $readability]);
//            } else {
//                $error = true;
//            }
//        }

        // Language detection
        if (is_null($record->language)) {
            $language = $datumBoxApi->LanguageDetection($record->title);
            if ($language) {
                $record->update(['language' => $language]);
            } else {
                $error = true;
            }
        }

        // Commercial detection
        if (is_null($record->commercial)) {
            $commercial = $datumBoxApi->CommercialDetection($record->title);
            if ($commercial) {
                $record->update(['commercial' => $commercial]);
            } else {
                $error = true;
            }
        }

        // Educational detection
        if (is_null($record->educational)) {
            $educational = $datumBoxApi->EducationalDetection($record->title);
            if ($educational) {
                $record->update(['educational' => $educational]);
            } else {
                $error = true;
            }
        }

        // Gender detection
        if (is_null($record->gender)) {
            $gender = $datumBoxApi->GenderDetection($record->title);
            if ($gender) {
                $record->update(['gender' => $gender]);
            } else {
                $error = true;
            }
        }

        $generatedNumbers = [];
        $oliverCount = OliverSimilarity::where('post_id', $record->id)->count();
        $oliverExclude = OliverSimilarity::where('post_id', $record->id)->pluck('similar_id')->toArray();
        $oliverExclude[] = $record->id;
        $possibleSimilarPosts = Sentiment::where('topic', $record->topic)
            ->whereNotIn('id', $oliverExclude)
            ->get();
        $max = count($possibleSimilarPosts) > 5 ? 5 : count($possibleSimilarPosts);
        for ($i = 0; $i < $max; $i++) {
            $rand = rand(0, count($possibleSimilarPosts)-1);
            while (in_array($rand, $generatedNumbers)) {
                $rand = rand(0, count($possibleSimilarPosts)-1);
            }
            $generatedNumbers[] = $rand;
            $possibleSimilarPost = $possibleSimilarPosts[$rand];
            $similarity = $datumBoxApi->DocumentSimilarity($record->title, $possibleSimilarPost->title);
            if ($similarity) {
                if ($oliverCount > 0) {
                    $leastSimilar = OliverSimilarity::where('post_id', $record->id)
                        ->orderBy('similarity')
                        ->first();
                    if ($leastSimilar->similarity < $similarity['Oliver']) {
                        $leastSimilar->update([
                           'similar_id' => $possibleSimilarPost->id,
                           'similarity' => $similarity['Oliver']
                        ]);
                    }
                } else {
                    $oliverSimilarity = OliverSimilarity::create([
                       'post_id' => $record->id,
                       'similar_id' => $possibleSimilarPost->id,
                       'similarity' => $similarity['Oliver']
                    ]);
                    $oliverSimilarity->save();
                }
            } else {
                $error = true;
            }
        }

        // Keywords extraction
        if (is_null($record->onegrams) and is_null($record->twograms)) {
            $ngrams = $datumBoxApi->KeywordExtraction($record->title, 2);
            if ($ngrams) {
                $onegrams = implode(', ',array_keys($ngrams[1]));
                $twograms = implode(', ',array_keys($ngrams[2]));
                $record->update([
                    'onegrams' => $onegrams,
                    'twograms' => $twograms
                ]);
            } else {
                $error = true;
            }
        }

        return view('detail', [
            'error' => $error,
            'record' => $record,
            'type' => $record->type
        ]);
    }
}
