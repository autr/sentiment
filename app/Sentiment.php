<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Sentiment extends Model {
    protected $table = 'sentiment';
    public $timestamps = false;

    protected $fillable = ['topic', 'simpleSentiment', 'afinnSentiment', 'afinnSentimentSimplified', 'datumboxSentiment',
                            'twitterSentiment', 'subjectivity', 'spam', 'adult', 'readability', 'language', 'commercial',
                            'educational', 'gender', 'onegrams', 'twograms'];

    /**
     * Returns similar posts
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function SimilarByOliver() {
        return $this->hasMany('App\OliverSimilarity', 'post_id', 'id')->orderBy('similarity', 'desc');
    }

    /**
     * Returns worded sentiment
     *
     * @param float $sentiment
     * @param bool $afinn
     * @return string
     */
    public function GetWordedSentiment(float $sentiment, bool $afinn = false) {
        if ($afinn) {
            if ($sentiment > 0.5) return 'positive';
            if ($sentiment > 0) return 'slightly positive';
            if ($sentiment == 0) return 'neutral';
            if ($sentiment < -0.5) return 'negative';
            if ($sentiment < 0) return 'slightly negative';
        } else {
            if ($sentiment > 0) return 'positive';
            if ($sentiment == 0) return 'neutral';
            if ($sentiment < 0) return 'negative';
        }
    }

    /**
     * Returns sentiment color for styling
     *
     * @param float $sentiment
     * @param bool $afinn
     * @return string
     */
    public function GetSentimentColor(float $sentiment, bool $afinn = false) {
        $wordedSentiment = $this->GetWordedSentiment($sentiment, $afinn);
        if ($wordedSentiment == 'positive') return 'green-text';
        if ($wordedSentiment == 'slightly positive') return 'light-green-text';
        if ($wordedSentiment == 'neutral') return 'text-warning';
        if ($wordedSentiment == 'slightly negative') return 'orange-text';
        if ($wordedSentiment == 'negative') return 'red-text';
    }

    /**
     * Returns spam as yes/no
     *
     * @param string $spam
     * @return string
     */
    public function GetFormattedSpam(string $spam) {
        if ($spam == 'nospam') {
            return 'no';
        } else {
            return 'yes';
        }
    }

    /**
     * Returns adult content as yes/no
     *
     * @param string $adult
     * @return string
     */
    public function GetFormattedAdult(string $adult) {
        if ($adult == 'noadult') {
            return 'no';
        } else {
            return 'yes';
        }
    }

    /**
     * Returns commercial content as yes/no
     *
     * @param string $commercial
     * @return string
     */
    public function GetFormattedCommercial(string $commercial) {
        if ($commercial == 'noncommercial') {
            return 'no';
        } else {
            return 'yes';
        }
    }

    /**
     * Returns educational content as yes/no
     *
     * @param string $educational
     * @return string
     */
    public function GetFormattedEducational(string $educational) {
        if ($educational == 'noneducational') {
            return 'no';
        } else {
            return 'yes';
        }
    }

    /**
     * Returns absolute counts for sentiment
     *
     * @param int $type
     * @param string $sentimentType
     * @return mixed
     */
    public function GetSentimentStats(int $type, string $sentimentType) {
        $output['positive'] = $this->where('type', $type)
            ->where($sentimentType.'Sentiment', '>', 0)->count();
        $output['neutral'] = $this->where('type', $type)
            ->where($sentimentType.'Sentiment', 0)->count();
        $output['negative'] = $this->where('type', $type)
            ->where($sentimentType.'Sentiment', '<', 0)->count();

        return $output;
    }

    /**
     * Returns similarity percentage of two sentiment types
     *
     * @param int $type
     * @param string $sentimentType1
     * @param string $sentimentType2
     * @param int|null $sentiment
     * @return float|int|null
     */
    public function GetSentimentSimilarity(int $type, string $sentimentType1, string $sentimentType2, int $sentiment = null) {
        if ($sentimentType1 == $sentimentType2) return null;

        $overallCount = $this->count();

        $suffix1 = $sentimentType1 == 'afinn' ? 'Simplified' : '';
        $suffix2 = $sentimentType2 == 'afinn' ? 'Simplified' : '';

        $sameCount = $this->where('type', $type)
            ->when(!is_null($sentiment), function ($q) use ($sentiment, $sentimentType1, $suffix1) {
                return $q->where($sentimentType1.'Sentiment'.$suffix1, $sentiment);
            })
            ->whereColumn($sentimentType1.'Sentiment'.$suffix1, $sentimentType2.'Sentiment'.$suffix2)->count();

        return ($sameCount/$overallCount) * 100;
    }

    /**
     * Returns topic counts
     *
     * @param int $type
     * @return mixed
     */
    public function GetTopicStatistics(int $type) {
        $topicStats = $this->where('type', $type)
            ->select('topic', DB::raw('count(*) as count'))
            ->groupBy('topic')
            ->orderBy('count', 'desc')
            ->get();

        return $topicStats;
    }

    /**
     * Returns language counts
     *
     * @param int $type
     * @return mixed
     */
    public function GetLanguageStatistics(int $type) {
        $languageStats = $this->where('type', $type)
            ->select('language', DB::raw('count(*) as count'))
            ->where('language', '!=', null)
            ->groupBy('language')
            ->orderBy('count', 'desc')
            ->get();

        $languageTotal = $this->where('type', $type)
            ->where('language', '!=', null)
            ->count();

        $output = [];
        foreach ($languageStats as $languageStat) {
            $output[$languageStat->language] = $languageStat->count / $languageTotal * 100;
        }

        return $output;
    }

    /**
     * Return text types percentages
     *
     * @param int $type
     * @return array
     */
    public function GetTextTypeStats(int $type) {
        $output = [];

        $spamCount = $this->where('type', $type)
            ->where('spam', 'spam')
            ->count();

        $spamTotal = $this->where('type', $type)
            ->where('spam', '!=', null)
            ->count();

        $output['spam'] = $spamCount / $spamTotal * 100;

        $adultCount = $this->where('type', $type)
            ->where('adult', 'adult')
            ->count();

        $adultTotal = $this->where('type', $type)
            ->where('adult', '!=', null)
            ->count();

        $output['adult'] = $adultCount / $adultTotal * 100;

        $commercialCount = $this->where('type', $type)
            ->where('commercial', 'commercial')
            ->count();

        $commercialTotal = $this->where('type', $type)
            ->where('commercial', '!=', null)
            ->count();

        $output['commercial'] = $commercialCount / $commercialTotal * 100;

        $educationalCount = $this->where('type', $type)
            ->where('educational', 'educational')
            ->count();

        $educationalTotal = $this->where('type', $type)
            ->where('educational', '!=', null)
            ->count();

        $output['educational'] = $educationalCount / $educationalTotal * 100;

        $subjectiveCount = $this->where('type', $type)
            ->where('subjectivity', 'subjective')
            ->count();

        $subjectiveTotal = $this->where('type', $type)
            ->where('subjectivity', '!=', null)
            ->count();

        $output['subjective'] = $subjectiveCount / $subjectiveTotal * 100;

        return $output;
    }
}
