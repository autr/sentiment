<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Afinn extends Model
{
    protected $table = 'afinn';
    protected $primaryKey = 'word';
    public $incrementing = false;
    public $timestamps = false;
}
