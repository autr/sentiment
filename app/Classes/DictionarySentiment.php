<?php
/**
 * Class for dictionary sentiment
 *
 * @author Martin Autersky
 */
namespace App\Classes;

use App\Afinn;
use App\NegativeWords;
use App\PositiveWords;

class DictionarySentiment {
    /**
     * Returns simple sentiment
     *
     * @param string $text
     * @return int
     */
    public function CountSimpleSentiment(string $text) {
        $words = $this->SplitSentence($text);

        $positiveCount = 0;
        $negativeCount = 0;

        foreach ($words as $word) {
            $positive = PositiveWords::find(strtolower($word));
            $negative = NegativeWords::find(strtolower($word));

            if($positive) {
                $positiveCount++;
            }

            if($negative) {
                $negativeCount++;
            }
        }

        if ($positiveCount + $negativeCount == 0) {
            return 0;
        } else {
            return $positiveCount / ($positiveCount + $negativeCount) > 0.5 ? 1 : -1;
        }
    }

    /**
     * Returns AFINN sentiment
     *
     * @param string $text
     * @return int
     */
    public function CountAfinnSentiment(string $text) {
        $words = $this->SplitSentence($text);

        $afinnCount = 0;

        foreach ($words as $word) {
            $afinn = Afinn::find($word);

            if($afinn) {
                $afinnCount += $afinn->rating;
            }
        }

        return $afinnCount / count($words);
    }

    /**
     * Returns simplified AFINN sentiment
     *
     * @param double $sentiment
     * @return int
     */
    public function GetSimplifiedAfinnSentiment(double $sentiment) {
        if ($sentiment > 0) {
            return 1;
        } else if ($sentiment < 0) {
            return -1;
        } else {
            return 0;
        }
    }

    /**
     * Splits sentence into words
     *
     * @param string $sentence
     * @return array|false|string[]|void
     */
    private function SplitSentence(string $sentence) {
        return preg_split( '/[^a-zA-Z0-9]+/', $sentence);
    }
}

