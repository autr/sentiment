@extends('layout.app')
@section('content')
    @if ($error)
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Warning!</strong> There was an error while processing the analysis. Some parts may be missing or marked as "unknown" as a result.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <div class="card">
        <div class="card-header default-color-dark white-text">
            <a class="btn btn-outline-default btn-sm" href="{{ route('preview', ['type' => $type]) }}"><i class="fas fa-caret-left"></i></a>
            "{{ $record->title }}"
        </div>
        <div class="card-body">
            <table class="table">
                <tbody>
                    <tr>
                        <th scope="row">Reddit URL</th>
                        <td class="w-75"><a href="{{ $record->permalink }}" target="_blank">{{ $record->permalink }}</a> </td>
                    </tr>
                    <tr>
                        <th scope="row">Topic</th>
                        <td class="w-75">{{ $record->topic }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Language</th>
                        <td class="w-75">{{ is_null($record->language) ? 'unknown' : $record->language }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th scope="row">Simple sentiment</th>
                        <td class="w-75 {{ $record->GetSentimentColor($record->simpleSentiment) }} font-weight-bold">{{ $record->GetWordedSentiment($record->simpleSentiment) }}</td>
                    </tr>
                    <tr>
                        <th scope="row">AFINN-111 sentiment</th>
                        <td class="w-75 {{ $record->GetSentimentColor($record->afinnSentiment, true) }} font-weight-bold">{{ $record->GetWordedSentiment($record->afinnSentiment, true) }}</td>
                    </tr>
                    <tr>
                        <th scope="row">DatumBox sentiment</th>
                        <td class="w-75 {{ $record->GetSentimentColor($record->datumboxSentiment) }} font-weight-bold">{{ $record->GetWordedSentiment($record->datumboxSentiment) }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Twitter sentiment</th>
                        <td class="w-75 {{ $record->GetSentimentColor($record->twitterSentiment) }} font-weight-bold">{{ $record->GetWordedSentiment($record->twitterSentiment) }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th scope="row">Spam</th>
                        <td class="w-75">{{ is_null($record->spam) ? 'unknown' : $record->GetFormattedSpam($record->spam) }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Adult content</th>
                        <td class="w-75">{{ is_null($record->adult) ? 'unknown' : $record->GetFormattedAdult($record->adult) }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Commercial text</th>
                        <td class="w-75">{{ is_null($record->commercial) ? 'unknown' : $record->GetFormattedCommercial($record->commercial) }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Educational text</th>
                        <td class="w-75">{{ is_null($record->educational) ? 'unknown' : $record->GetFormattedEducational($record->educational) }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th scope="row">Subjectivity</th>
                        <td class="w-75">{{ is_null($record->subjectivity) ? 'unknown' : $record->subjectivity }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Gender</th>
                        <td class="w-75">{{ is_null($record->gender) ? 'unknown' : $record->gender }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th scope="row">1-grams</th>
                        <td class="w-75">{{ is_null($record->onegrams) ? 'unknown' : $record->onegrams }}</td>
                    </tr>
                    <tr>
                        <th scope="row">2-grams</th>
                        <td class="w-75">{{ is_null($record->twograms) ? 'unknown' : $record->twograms }}</td>
                    </tr>
                </tbody>
            </table>
            <br>
            <h3>Similar posts in {{ $record->topic }}</h3>
            <table class="table">
                <tbody>
                    @foreach($record->SimilarByOliver as $similar)
                        <tr>
                            <th scope="row" class="w-90">
                                <a href="{{ route('detail', ['id' => $similar->SimilarPost->id]) }}" target="_blank">{{ $similar->SimilarPost->title }}</a>
                            </th>
                            <td><a href="{{ $similar->SimilarPost->permalink }}" target="_blank">Reddit URL</a></td>
                            <td>{{ number_format($similar->similarity*100,2) }}%</td>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
