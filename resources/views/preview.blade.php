@extends('layout.app')
@section('content')
    <div class="card">
        <div class="card-header default-color-dark white-text">
            Preview for {{ $typeName }}
            <a class="btn btn-outline-default btn-sm" href="{{ route('statistics', ['type' => $type]) }}">Statistics</a>
        </div>
        <div class="card-body">
            <table class="table table-hover">
                <thead class="default-color-dark white-text">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Topic</th>
                    <th scope="col">Score</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @php($counter = 1)
                @foreach($records as $record)
                    <tr>
                        <th scope="row">{{ $counter++ }}</th>
                        <td class="w-75">{{ $record->title }}</td>
                        <td>{{ $record->topic }}</td>
                        <td>{{ $record->score }}</td>
                        <td><a href="{{ route('detail', ['id' => $record->id]) }}" class="btn btn-outline-default btn-sm"><i class="fas fa-eye"></i></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
