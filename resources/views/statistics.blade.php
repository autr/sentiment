@extends('layout.app')
@section('content')
    <div class="card">
        <div class="card-header default-color-dark white-text">
            <a class="btn btn-outline-default btn-sm" href="{{ route('preview', ['type' => $type]) }}"><i class="fas fa-caret-left"></i></a>
            Statistics for {{ $typeName }}
        </div>
        <div class="card-body">
            <h3>Absolute sentiment counts</h3>
            <table class="table">
                <thead class="default-color-dark white-text">
                <tr>
                    <th scope="col"></th>
                    <th class="text-center" scope="col">Positive</th>
                    <th class="text-center" scope="col">Neutral</th>
                    <th class="text-center" scope="col">Negative</th>
                </tr>
                </thead>
                <tbody>
                @foreach($sentimentStats as $sentimentType => $stats)
                    <tr>
                        <th scope="row">{{ ucfirst($sentimentType) }} sentiment</th>
                        <td class="text-center">{{ $stats['positive'] }}</td>
                        <td class="text-center">{{ $stats['neutral'] }}</td>
                        <td class="text-center">{{ $stats['negative'] }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @foreach($sentimentSimilarity as $scope => $sentimentSimilaritySpecific)
                <br>

                <h3>Same sentiment percentage - {{ $scope }}</h3>
                <table class="table">
                    <thead class="default-color-dark white-text">
                    <tr>
                        <th scope="col"></th>
                        <th class="text-center" scope="col">Simple</th>
                        <th class="text-center" scope="col">Afinn</th>
                        <th class="text-center" scope="col">Datumbox</th>
                        <th class="text-center" scope="col">Twitter</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sentimentSimilaritySpecific as $sentimentType => $similarity)
                        <tr>
                            <th class="default-color-dark white-text" scope="row">{{ ucfirst($sentimentType) }}</th>
                            @foreach($similarity as $percentage)
                                @if(is_null($percentage))
                                    <td class="text-center">x</td>
                                @else
                                    <td class="text-center">{{ number_format($percentage, 2) }}</td>
                                @endif
                            @endforeach
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endforeach
            <br>

            <h3>Topic percentages</h3>
            <table class="table">
                <thead class="default-color-dark white-text">
                <tr>
                    @foreach($topicStats as $topicStat)
                        <th class="text-center" scope="col">{{ ucfirst($topicStat->topic) }}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                    <tr>
                        @foreach($topicStats as $topicStat)
                            <td class="text-center">{{ $topicStat->count / 1000 * 100 }}</td>
                        @endforeach
                    </tr>
                </tbody>
            </table>
            <br>

            <h3>Language percentages</h3>
            <table class="table">
                <thead class="default-color-dark white-text">
                <tr>
                    @foreach($languageStats as $language => $percentage)
                        <th class="text-center" scope="col">{{ $language }}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                <tr>
                    @foreach($languageStats as $language => $percentage)
                        <td class="text-center">{{ $percentage }}</td>
                    @endforeach
                </tr>
                </tbody>
            </table>
            <br>

            <h3>Text type percentages</h3>
            <table class="table">
                <thead class="default-color-dark white-text">
                <tr>
                    @foreach($textStats as $textType => $percentage)
                        <th class="text-center" scope="col">{{ ucfirst($textType) }}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                <tr>
                    @foreach($textStats as $textType => $percentage)
                        <td class="text-center">{{ $percentage }}</td>
                    @endforeach
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
